# Proposition

**Stéphane Deudon** sera ton MOA et **Jérémie Ferry** ton MOE.

L'objectif est de créer une application qui correspond aux attendus de l'éducation nationale mais qui n'existe pas encore au sein de Primtux.
Pour ce faire, nous allons te fournir le besoin et tu seras libre sur la création.

# Etapes

1. Rédaction de spécifications fonctionnelles et techniques incluant des mockups des futures écrans.
2. Présentation et validation de l'aspect fonctionnel par Stephane et de la technique par Jérémie.
2. Mise en place d'un POC : premier essai non abouti de l'exerciseur.
3. Validation du MOA et MOE.
4. Finalisation du projet.
5. Validation finale.

## Le besoin

Créer une application pour le CM2 sur le thème : Communiquer d’un bout à l’autre du monde grâce à Internet

1. À partir des usages personnels de l’élève de l’Internet et des
activités proposées pour développer la compétence
« S’informer dans le monde du numérique », on propose à
l’élève de réfléchir sur le fonctionnement de ce réseau. On
découvre les infrastructures matérielles nécessaires au
fonctionnement et au développement de l’Internet. Ses usages
définissent un nouveau rapport à l’espace et au temps
caractérisé par l’immédiateté et la proximité. Ils questionnent la
citoyenneté. On constate les inégalités d’accès à l’Internet en
France et dans le monde.
Complété par l'éducation civique: Prendre conscience des enjeux civiques de
l'usage du numérique et des réseaux sociaux.

2. La notion de bien commun.
Avoir conscience de sa responsabilité
individuelle.

Actuellement, la réponse au besoin existe au format papier : voir les 3 pdfs.
L'objectif est d'avoir une version interactive impliquant l'enfant dans la démarche d'apprentissage.

## Un exemple de scénario

La démarche pourrait être des questions à la manière du jeu dont tu est le héros (https://fr.wikipedia.org/wiki/Un_livre_dont_vous_%C3%AAtes_le_h%C3%A9ros).
Ca pourrait te forcer à créer une mini machine à état fini.
Pour évité que l'enfant s'appuie sur un apprentissage bête de la réponse à chaque étape, on pourrait partir sur un panel de questions "possibles" (donc choisi aléatoirement et non répété) pour chacune des étapes.

Il pourrait y avoir des pièges.
Par exemple, une pub déguisée.

## La partie graphique

L'objectif est de réutiliser des composants dans une démarche de design system.
Ce dernier est déjà partiellement au point. Il ne suffira que de l'étoffer.

Pour ce qui est des images, tu n'auras bien évidement pas à les créer mais donner un descriptif du dessin attendu serait un plus.
Tu pourras également glaner des images sur le net.

Nous nous assurerons de compléter nous même avec des contenus graphiques.

## Code source ouvert

Tout ce que tu produiras sera open-source donc tu as le choix entre une license BSD, MIT ou GNU.

## contraintes techniques

- Utilisation de javascript vanilla. Tout ajout de librairie javascript devra être justifié.
- Projet responsive : devra être consultable sur le maximum de plate-forme.
- Mode tactile.
- Peu gourmand en ressources.
- Utilisation au maximum du design system.
- Pilotage via des tests unitaires et d'intégration [Cypress](https://framagit.org/mothsart/rfc-primtux-web-app/-/blob/master/lexique.md#cypress)

En somme, se rapprocher au plus possible des contraintes énumérés ici : [RFC de Primtux](https://framagit.org/mothsart/rfc-primtux-web-app)

## Contrepartie

Ton nom sera bien évidement cité dans les crédits et on communiquera sur ton implication.
Si besoin, nous pourrons aussi attesté à ton école de la démarche, de l'accompagnement et du sérieux que tu auras fait part pour mener le projet à son terme.

