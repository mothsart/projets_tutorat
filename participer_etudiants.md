# Participer à Primtux

## Intro

La distribution Primtux est riche et varié.
Pour y participer, il ne faut pas nécessairement connaitre sur le bout des doigts toutes les technologies employés mais au moins certaines.
Les qualités requisent sont plus :
- de la rigueur : une vrai démarche de développement.
- de l'écoute.
- du pragmatisme.
- de la curiosité.
- de la veille informatique.

# Les technologies

1. Bonnes connaissances du web en frontend : html, javascript, css.
2. Python 3 et Flask pour le backend.
3. Connaissances en SQL.
4. Bonnes connaissances autour de linux : systemd, inotify, dbus, nginx, shell etc.
5. Debian, apt et création de .deb
6. Utilisation de git et gitlab.

## Les défits possibles

- Création d'un moteur de recherche.
- Synthèse vocale meilleur que l'actuel GSpeech.
- Aide interactive.
- CI/CD, tests unitaires et d'intégrités.
- Création d'un store.
- Reprende le projet Grammalect.
- Un hack debian pour forcer l'harmonisation des mdp dans CTParental.
- Mise en place du nouveau site Internet.
- Travaux d'optimisation : réduire la taille de l'iso, de la consommation en Ram, Cpu etc.
- Statistiques d'utilisation et collectes et de données.
- Créer des exerciceurs webs.
